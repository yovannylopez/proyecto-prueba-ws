<?php

namespace App;

use App\Lugar;
use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $fillable = [
        'departamento'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function lugares()
    {
        return $this->hasMany(Lugar::class);
    }
}
