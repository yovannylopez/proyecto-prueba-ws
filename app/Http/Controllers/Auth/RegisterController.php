<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        // $this->validator($request->all())->validate();
        $datos = $request->all();

        // En esta validación no colocaré la confirmación de la contraseña
        $validator = Validator::make($datos, [
            'nombre' => 'required|string|min:3',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:3'
        ]);

        if ($validator->fails()) {
            return response()->json(['mensaje' => 'información no procesada'], 422);
        }

        $datos['password'] = bcrypt($request['password']);
        // $datos['api_token'] = str_random(40);

        $user = User::create($datos);

        $this->guard()->login($user);
        $user->generateToken();

        return response()->json($user, 201);
    }
}
