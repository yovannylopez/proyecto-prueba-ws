<?php

namespace App\Http\Controllers;

use App\Lugar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LugarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lugares = Lugar::join("departamentos", "lugares.departamento_id", "=", "departamentos.id")
                ->get();

        return response()->json($lugares, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /***
         * También puede hacer un request de todo, pero especifica
         * los campo que realmente son obligatorios
         */

//         $all    = $request->only([
//             'name','latitude','longitude','x','y','image','description'
//         ]);

//         $val    = Validator::make($all, [
//             'name'          => 'required|string|max:100',
        // //            'latitude'      => 'required|numeric|between:24.35429,24.59895',
        // //            'longitude'     => 'required|numeric|between:54.2522,54.66419',
//             'latitude'      => 'required',
//             'longitude'     => 'required',
//             'image'         => 'required|file|image',
//             'description'   => 'nullable|string'
//         ]);

        $lugares = Lugar::create([
            'nombre' => $request->input('nombre'),
            'descripcion' => $request->input('descripcion'),
            'imagen' => $request->imagen->store(''),
            'departamento_id' => $request->input('departamento_id')
        ]);

        return response()->json($lugares, 201);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$lugar = Lugar::findOrFail($id);

        $lugar = Lugar::join("departamentos", "lugares.departamento_id", "=", "departamentos.id")
                ->findOrFail($id);

        return response()->json($lugar, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lugar = Lugar::find($id);

        /*Esta línea permite que se elimine la imagen cuando
        se elimina un lugar*/
        Storage::delete($lugar->imagen);

        $lugar->delete();

        return response()->json(['mensaje' => 'eliminación correcta'], 200);
    }
}
