<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class AdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->rol == 'ADMINISTRADOR') {
            return $next($request);
        } else {
            return response()->json(['mensaje' => 'usuario no autorizado'], 401);
        }
    }
}
