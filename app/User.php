<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /*****
     * Si en algún momento tiene problemas con la migración,
     * puede agregar la siguiente propiedad
     * protected $table = 'users';
     */

    // aquí se cambia el name por nombre
    protected $fillable = [
        'nombre', 'email', 'password', 'role'
    ];


    /***********************************
     * Ojo con esto, debe revisar bien en el proyecto prueba
     * cuales son los campos que deben aparecer en las respuestas,
     * para ello este atributo. Lo que se oculta aquí no aparece
     * en el json de salida (response)
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at'
    ];

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }
}
