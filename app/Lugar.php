<?php

namespace App;

use App\Departamento;
use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    /****
     * En este caso es obligatorio colocar el nombre de la tabla lugares
     * por defecto el framework al crear el modelo Lugar, genera el plural "lugars"
     * Sino coloca este atributo, no le va a permitir hacer nada
     */
    protected $table = 'lugares';

    protected $fillable = [
        'nombre', 'descripcion', 'imagen', 'departamento_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function departamento()
    {
        return $this->belongsTo(Departamento::class);
    }
}
