<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Route::group();



// se define en forma de prefijo la versión 1
Route::group(['prefix' => 'v1'], function () {
    // Registrar un usuario con rol visitante
    Route::post('registro', 'Auth\RegisterController@register');

    // Login de un usuario registrado (turista, admininistrador)
    Route::post('auth/login', 'Auth\LoginController@login');

    Route::apiResource('lugares', 'LugarController', ['only' => ['index']]);


    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('auth/logout', 'Auth\LoginController@logout');

        Route::apiResource('lugares', 'LugarController', ['except' => ['index']]);

        Route::group(['middleware' => 'auth.admin'], function () {
            Route::get('users', 'UserController@index');
            Route::apiResource('departamentos', 'DepartamentoController');
        });
    });
});


// Route::group(['middleware' => 'isAdmin'], function () {
//     Route::apiResource('users', 'UserController');
//     Route::apiResource('departamentos', 'DepartamentoController');
// });
