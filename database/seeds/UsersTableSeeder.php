<?php

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class UsersTableSeeder extends Seeder
{
    /* Esta clase debe invocar una sola vez desde para almacenar
    dicha información en la tabla de users
    Se crea con el comando php artisan make:seeder UsersTableSeeder*/
    public function run()
    {
        User::truncate();

        User::create([
            'nombre' => 'administrador',
            'email' => 'admin@colombia.co',
            'password' => bcrypt('adminpass'),
            'rol' => 'ADMINISTRADOR'
        ]);

        User::create([
            'nombre' => 'john doe',
            'email' => 'johndoe@gmail.com',
            'password' => bcrypt('johnpass'),
            'rol' => 'TURISTA'
        ]);
    }
}
