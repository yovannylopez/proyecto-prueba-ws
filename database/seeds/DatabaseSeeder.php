<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{

    /* Este método se descomenta para poder hacer el proceso
    de migración del seed de users.
    Tenga presente que el seed lo puede hacer paralelamente con la
    migración por medio del comando php artisan migrate --seed
    o de manera individual después de la migración con el comando
    php artisan db:seed*/
    public function run()
    {
        $this->call(UsersTableSeeder::class);
    }
}
