<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /* En este método se cambia el nombre de atributo name por nombre,
    se agreaga el atributo rol para definir en forma de constantes
    los roles administrador y turista, dejando por defecto el rol turista*/
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('api_token')->unique()->nullable();
            $table->rememberToken();
            $table->enum('rol', ['ADMINISTRADOR', 'TURISTA'])->default('TURISTA');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
